% -------------------------------------------------------------------------
% SPIKE_O toolbox - DemoBB.m
%
% This script demonstrates/tests the branch-and-bound algorithm for
% optimization of Rosenbrock's banana function. Note that the
% branch-and-bound algorithm is not the best algorithm for this problem.
% The banana function is merely used as a simple test example.
%
% The script assumes that the toolbox is correctly installed and added to the
% path. See Readme for instructions.
% 
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-01-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

disp('1. Setup and vizualization of the problem')
% -----------------------------------
% Setup problem
a   =   1   ;
b   =   0   ;
c   =   100 ;
model.a = a ;
model.b = b ;
model.c = c ;

% -----------------------------------
% Vizualize problem
tx      =   -2:.01:2            ;
ty      =   -1:.01:3            ;
[X,Y]   =   meshgrid(tx,ty)     ;
x       =   X(:)                ;
y       =   Y(:)                ;

z       =   BananaFun([x y],model)    ;

imag    =   reshape(z,[length(tx) length(ty) ]);
figure, surf(tx(:),ty(:),(imag),'linestyle','none')
disp('      The plotted figure plots Rosenbrock''s banana function')
disp('	pause')
pause

% -----------------------------------
% Test bounding procedures:
disp('2. Demonstrating the provided bounding functions')

% Set interval bounds:
IntX = [ -3 -3 ;  3 3  ];
% test bounds:
[LB,UB]     =   BananaBounds(model,IntX) ;
disp([ '      Lower bound: ' num2str(LB,'%5.6f')])    
disp([ '      Upper bound: ' num2str(UB,'%5.6f')])
disp('	pause')
pause

% -----------------------------------
% Optimization
disp('3. Branch-and-bound algorithm')

disp('')
disp('	Colour code: ')    
disp('      Blue    = fathomed node')    
disp('      Pink    = verified solution (upper bound = lower bound)')    
disp('      Black   = selected solution')    
disp('  Please take note of above colour codes to understand subsequent vizualization of the branch-and-bound algorithm')    
disp(' paused')
pause

% set optimization options:
options             =   OptimsetBB      ;
options.boundfun    =   @BananaBounds   ;
options.boundlow    =   @BananaLow      ;
options.boundupp    =   @BananaUpp      ;
warning('off','MATLAB:declareGlobalBeforeUse')

% actual BB algorithm:
[Tree,model]        =   BB(model,options,IntX);

Interval= model.IntX ;
Solution= model.SolX ;

disp('')
disp('	Obtained best solution: ')            
disp([ '      x: ' num2str(Solution(1)','%5.6f')])    
disp([ '      y: ' num2str(Solution(2)','%5.6f')])


