function [LB,UB]=BananaBounds(model,IntX,a1,a2,a3) 

% -------------------------------------------------------------------------
% SPIKE_O toolbox - BananaBounds.m
%
% This function evaluates the banana function
% 
% Syntax:
%   [LB,UB] = BananaBounds(model,IntX) 
%
% Inputs:
%   model   :   Structure describing the parameters of the banana function
%   IntX    :   Polyhedral set of feasible parameters
%
% Outputs:
%   LB      :   lower bound banana function value
%   UB      :   upper bound banana function value
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2013-05-31
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

[LB] = BananaLow(model,IntX);
[UB] = BananaUpp(model,IntX);