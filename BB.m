function [Tree,model,Iteration]= BB(model,options,IntX,DistX,X0)

% -------------------------------------------------------------------------
% SPIKE_O toolbox - BB.m
%
% This function executes the branch-and-bound algorithm.
%
% Syntax:
%   [Tree,model,Iteration]	=	BB(model,options,IntX,DistX,X0)
%
% Inputs:
%   model       :   mathematical problem as a structure
%   options     :   [optional] options for algorithm execution
%   IntX        :   Root set
%   DistX       :   [optional] Distance constraints
%   X0          :   [optional] Initial guess
%
% Outputs:
%   Tree        :   Solution tree
%   model       :   updated model with solution as field 'SolX' and
%                   selected node as field 'IntX'
%   Iterations  :   Number of iterations required until termination
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-01-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% =========================================================================
% I. Setup and initialization

if nargin<2 || isempty(options)
    default.boundfun    =   @Bounds      ;
    default.boundlow    =   @Boundlow    ;
    default.boundupp    =   @Boundupp    ;
    default.earlystop   =   1               ;
    default.node        =   2               ;
    default.var         =   1               ;
    default.rounding    =   0               ;
    default.branching   =   1               ;
    default.splitting   =   3               ;
    default.resolution  =   .01             ;
    default.maxiter     =   1e6             ;
    default.verbose     =   2               ; % values: 0-2
    default.verify      =   2               ; % values 
    default.graph       =   2               ;
    default.defaultsplit   =   []               ;
    options             =   default         ;
end

if isfield(model,'type')
    SCStype = strcmp(model.type,'SCS') ;
else
    SCStype =false;
end

global multicrit
if isfield(model,'multicrit')
    multicrit = model.multicrit ;
else
    multicrit = 1 ;
end

if SCStype
    DefIntX = DefaultIntX(model) ;
    if nargin <3 || isempty(IntX)
       IntX = DefIntX; 
    else
        IntX(1,:)  = max([ IntX(1,:); DefIntX(1,:)]);
        IntX(2,:)  = min([ IntX(2,:); DefIntX(2,:)]);
    end
end
nib         =   size(IntX,2)        ;
% IntX

% save original input model (Generative Trend Object)
model0       =   model             ;
%TimeMin     =   min(tk(2:end)-tk(1:end-1))      ;
%TimeMax     =   max(tk(2:end)-tk(1:end-1))      ;

% ----------------------------------------------------
% Get options
boundfun            =   options.boundfun    ;   % function to be used to compute bounds on solution
boundupp            =   options.boundupp    ;   % function to be used to upper bounds on solution
boundlow            =   options.boundlow    ;   % function to be used to upper bounds on solution
method              =   options.node        ;   % method of node selection for branching: 1: best lower bound; 2: best upper bound; 3: depth-first, 4: breadth-first
graph               =   options.graph       ;
earlystop           =   options.earlystop   ;
Resolution          =   options.resolution  ;   % time resolution (only important for discontinuities)
MaxIter             =   options.maxiter     ;   % maximum iterations
Verbose             =   options.verbose     ;   % command line outputs on or off
Verify              =   options.verify      ;   % use shape verification when available


if Verbose , disp('BB algorithm started'), end

% ----------------------------------------------------
% SPECIFICATIONS FOR LEAF GENERATION
Genleafoptions.rounding         =   options.rounding        ;
Genleafoptions.branching        =   options.branching       ; % method of branching
Genleafoptions.splitting        =   options.splitting       ;
Genleafoptions.defaultsplit     =   options.defaultsplit    ;   % default splits


BestObj =   inf ;
if earlystop
    if isfield(model,'BestObj')
        BestObj =   model.BestObj ;
    end
end

% interval for locations of interior bounds 
if nargin<4 || isempty(DistX),  
    if SCStype
        DistX   =   DefaultDistX(model)         ;   % set default distance ranges
    else
        DistX   = [];
    end
else
    DistX   =   min(max(DistX,-Inf),Inf)    ;   % reduce range where possible
end
% -------------------------------------------------------------------------
% PREPARATIONS

Indices     % necessary index variables

outsidefirst = false;
if outsidefirst
    % generate vector vv: indicates default order of variables. This is
    % done so that the transition variables at the 'outside' are handled
    % first. This is a heuristic rule which sometimes leads to fast
    % exclusion of extremal regions in the solution space if these are
    % indeed suboptimal
    ni1             =   floor(nib/2)        ;
    ni2             =   nib-ni1             ;
    vv(1:2:nib)     =   1:ni2               ;
    vv(2*(1:ni1))   =   fliplr(ni2+1:nib)   ;
else
    vv = 1:nib;
end

% -------------------------------------------------------------------------
% Variable resolution
DeltaX      =   Resolution*ones(1,nib)      ;
RangeX      =   Ranger(IntX)                ;

% reset MaxIter when possible
splitting = Genleafoptions.splitting;
Bins        =   (IntX(2,:)-IntX(1,:))./DeltaX           ;   % width in intervals of width Dcrit
BinMax      =   ceil(log(Bins)/log(splitting))          ;   % max no of binary decisions for each variable

MaxIter     =   min(MaxIter,prod(splitting.^BinMax))    ;
if Verbose>=2, disp(['Maximum iterations set to: ' num2str(MaxIter)])  , end

% pause
IntX0       =   IntX                        ;   % save original variable intervals
opt         =   find(RangeX>0)              ;
nopt        =   sum(opt>0)                  ;
Vol         =   prod(RangeX(opt))           ;   % (knot-based) volume for root set
City        =   sum(RangeX(opt))            ;   % (knot-based) city distance root set
Vol0        =   Vol                         ;   % reference volume
City0       =   City                        ;   % reference city distance
% IntX0
% ----------------------------------------
% make graphs
if graph

    figure();
    Lim     =   IntX(:,opt)     ;
    
    if nopt==1
        if isfield(options,'graphtype')
            graphtype = options.graphtype;
        else
            graphtype = 3;
        end
        
        nsub = 1;
        
        subplot(nsub,1,1)
        if exist('AxisPrep'), AxisPrep, end
        hold on
        Xlim    =   Lim(:,1)    ;
        Ylim    =   [ 0; 1]     ;
        
        switch graphtype
            case 1
                set(gca,'Xlim',Xlim,'Ylim',Ylim)
                drawnow
            case 2
                set(gca,'Xlim',Xlim )
                drawnow
			case 3
				%subplot(2,1,1), hold on
                %set(gca,'Xlim',Xlim,'tickdir','out' )
                %subplot(2,1,2), hold on
                set(gca,'Xlim',Xlim ,'tickdir','out')
				%set(gca,'color',ones(1,3)*.5,'tickdir','out')
                drawnow
        end
        
    else
        graphtype = 2;
        
%         AxisPrep
%         hold on
       
        low = min(IntX,[],1);
        upp = max(IntX,[],1);
        han1 = plot([1:nib ; 1:nib],[low; upp],'ko-','MarkerFaceColor','k');
        Xlim    =   [.5 nib+.5]    ;
        Ylim    =   [ min(low) max(upp) ]   ;
        
        set(gca,'Xlim',Xlim,'Ylim',Ylim)
        drawnow
    end

end
% ----------------------------------------
if Verify>=0
    if SCStype
        model       =   AssessVerifiability(model,IntX)  ;
    end
end
% IntX
% -------------------------------------------------------------------------
% INITIALIZE BnB

Termination =   0   ;   % algorithm termination: 1/0 true/false
Iteration   =   0   ;   % iteration count: integer

if Verbose>=2, disp(['Branching step: ' num2str(Iteration) ]), end  % display

% -----------------------------
% Compute bounds on root node
[LB,UB]     =   boundfun(model,IntX0,DistX)   ;    % evaluate generic bounds on root

% improve upper bound by using provided solution if possible
if nargin>=5 && ~isempty(X0)
    % following line changed: all boundupp functions should have IntX0 as
    % additional input
    X0  =   min(max(IntX0(1,:),X0),IntX0(2,:))  ;
    UBinit  =   boundupp(model,IntX0,X0)    ;
    UB      =   min(UBinit,UB)              ;
end
OB1  = UB ;
model.OB1 = OB1 ;

% -----------------------------
% Gather information of root node
%               parent      level   live    LB  UB   Volume City solution
Root        =   [   0       0       1       LB  UB  Vol0    City0 IntX(:)' ]   ;
ComputeVolumes(Root,Verbose,Vol0,model)    ;

% -----------------------------
% Initialize tree matrix and record keeping
nRoot       =       length(Root)                ;   % number of values to be stored per node
Tree        =       nan(min(MaxIter,1e6),nRoot)          ;   % initialize tree matrix
Tree(1,:)   =       Root                        ;   % populate first item (root node)
nTree       =       1                           ;   % number of populated nodes
Record      =   [   Iteration LB UB 1 1 ]       ;   % matrix to keep track of solution quality; not necessary for algorithm

IterDisplay = 1 ;
TotVol = Vol0;
TotCity = City0;

% =========================================================================
% II. Actual algorithm

while ~Termination
    
    Iteration   =   Iteration +1                ;   % Update iteration count
    % 0. checks
    %assert(all(Tree(:,3)<=Tree(:,4)+1e-3),'Some UB lower than LB')
    
    % 1. locate node to split
    % heuristic: live node with minimal LB
    Alive       =   find(Tree(1:nTree,iLive)==1)    ; % find nodes that are alive and for which knot resolution is not achievedTem
    if isempty(Alive)   % if no such nodes
        Alive   =   find(Tree(1:nTree,iLive)>=1)    ; % find nodes that are alive
    end
    % stop algorithm under these conditions
	if multicrit==1
		Term1       =   all(Tree(1:nTree,iLB)>BestObj)  ;
	else
		Term1 		= 	false 	; 
	end
		
    Term2           =   isempty(Alive)                  ;
    Term3           =   Iteration>MaxIter               ;
    
    Termination     =   any([Term1 Term2 Term3 ])  ;
    
    if ~Termination 
        
        if mod(Iteration,11)==0
            clc
        end
        if Verbose>=2 && mod(Iteration,IterDisplay)==0, disp(['Branching step: ' num2str(Iteration) ]), end  % display
        
        % -----------------------------------------------------------------
        % 1. select node to be branched
                
        alt = mod(Iteration,multicrit)+1 ;
        
        switch method
            case 1 % best upper bound
                [MinUB]     =   min(Tree(Alive,iUB(alt)),[],1)           ;   % find minimum upper bound in live nodes
                diffloc     =   find(Tree(Alive,iUB(alt))==MinUB)        ;   % find nodes with minumum upper bound
%                 [oo,loc]    =   max(Tree(Alive(diffloc),iVol),[],1) ;   % among nodes with minimum upper bound, find the one with maximum volume
                branch      =   Alive(diffloc((1)))                 ;   % select this one as branch
            case 2 % best lower bound
                [MinLB]     =   min(Tree(Alive,iLB(alt)),[],1)           ;   % find minimum lower bound in live nodes
                diffloc     =   find(Tree(Alive,iLB(alt))==MinLB)        ;   % find nodes with minumum lower bound
                [oo,loc]    =   min(Tree(Alive(diffloc),iUB(alt)),[],1)  ;   % among nodes with minimum bound, find the one with minimum upper bound
                branch      =   Alive(diffloc(loc(1)))                 ;   % select this one as branch
            case 4 % depth-first
                branch      =   Alive(find(Tree(Alive,iParent)==Tree(Alive(end),iParent),1,'last'))    ;
            case 3 % breadth-first
                 %AliveIndex = find(Alive) ;
%                 Volumes     =   (Tree(AliveIndex,iVol))                  ;
%                 
%                 %Tree(Alive,index:end)
                branch      =   Alive(find(Tree(Alive,iParent)==Tree(Alive(1),iParent),1,'first'))    ;
                
%                 Level       =   min(Tree(Alive,iLevel))                 ;
%                 Select      =   Alive(find(Tree(Alive,iLevel)==Level))  ;
%                 Volume      =   max(Tree(Select,iVol))                  ;
%                 Select      =   Select(find(Tree(Select,iVol)==Volume))  ;
%                 UBs         =   Tree(Select,iUB)    ;
%                 [MinUB]     =   min(UBs(:),[],1)           ;   % find minimum lower bound in live nodes
%                 diffloc     =   find(any(ismember(Tree(Select,iUB),MinUB),2))        ;   % find nodes with minumum lower bound
%                 LBs         =   Tree(Select(diffloc),iLB)    ;
%                 [MinLB]    =   min(LBs(:),[],1)  ;   % among nodes with minimum bound, find the one with minimum upper bound
%                 loc         =   find(any(ismember(Tree(Select(diffloc),iLB),MinLB),2))        ;   % find nodes with minumum lower bound
%                 branch      =   Select(diffloc(loc(1)))                 ;   % select this one as branch
            case 5 % max difference
                D           =   Tree(Alive,iUB)-Tree(Alive,iLB)     ;
                [MaxD]      =   min(D,[],1)                         ;   % find maximum difference
                diffloc     =   find(D==MaxD)                       ;   % find nodes with minumum lower bound
                [oo,loc]    =   min(Tree(Alive(diffloc),iLB),[],1)  ;   % among nodes with minimum bound, find the one with minimum upper bound
                branch      =   Alive(diffloc(loc(1)))                 ;   % select this one as branch
        end
        level               =   Tree(branch,iLevel)         ;   % level of parent

        % -----------------------------------------------------------------
        % 2. branching
        if SCStype
            Leafs = GenerateLeafs(Tree,branch,index,nib,vv,Verbose,Genleafoptions,DeltaX,model );
        else
            Leafs = GenerateLeafs(Tree,branch,index,nib,vv,Verbose,Genleafoptions,DeltaX,model);
        end
        
        % -----------------------------------------------------------------
        % 3. Evaluate objective bounds in each of the new leaves
        % and execute fathoming where necessary
        n_leaf = length(Leafs);
        for i_leaf=1:n_leaf
            % 3A. Evaluate objective bounds
            if Verbose>=3 && mod(Iteration,IterDisplay)==0, disp(['  Analyzing leaf - ' num2str(i_leaf) ' of ' num2str(n_leaf) ]), end                  % display
            Xleaf           =   Leafs{i_leaf,1}                 ;   % copy branch intervals for leaf
            iTree           =   nTree+i_leaf                    ;
            [LB,UB,Xapp]    =   ProcessLeaf(Xleaf,IntX0,boundfun,model,DistX,Verify)        ;
            Tree            =   StoreLeaf(Tree,iTree,branch,level,opt,LB,UB,Xapp,Xleaf,model)     ;
            Tree            =   FathomLeaf(Tree,iTree,iLive,LB,UB,Xleaf,DeltaX,model)       ;
            
            % vizualization
            if graph 
				if graphtype==1
					VisualLeaf(Tree,iTree,iLive,index,opt,nopt,graphtype)  ;
				elseif graphtype==3
                    if length(UB)==1
                        VisualLeaf(Tree,iTree,iLive,index,opt,nopt,graphtype)  ;
                        subplot(2,1,1)
                        VisualTreeNodes(Tree,Iteration);
                    else
                        disp('here')
                        VisualTreeNodes(Tree,Iteration);
                    end
				end
            end
            
        end

        % -----------------------------------------------------------------
        % 4. Fathom by dominance
        Tree(branch,iLive)          =   0                           ;   % parent is set dead        
        nTree                       =   nTree+n_leaf                ;   % number of populated nodes
        Alive                       =   find(Tree(1:nTree,iLive))   ;   % find live nodes
		fathom                      =   Fathoming(Tree,Alive,[],multicrit)       ;   % find nodes to be fathomed by dominance
        Tree(Alive(fathom),iLive)   =   0                           ;   % set fathomed nodes as dead
        AliveN                     	=   find(Tree(1:nTree,iLive))   ;
		
%         nAliveN        =   length(AliveN) ;
        % -----------------------------------------------------------------
        % vizualization
%        pause
        if graph
            switch graphtype
                case 1
                    str = 'b-' ;
                    VisualTree(Tree,Alive(fathom),str,index,opt,nopt, graphtype,model)    ;
                case 2
                    if mod(Iteration,IterDisplay)==0
                        
                        hold off
                        low = min(Tree(AliveN,index:2:end));
                        upp = max(Tree(AliveN,index+1:2:end));
                        han1 = plot([1:nib ; 1:nib],[low; upp],'ko-','MarkerFaceColor','k');
                        hold on
                        set(gca,'Xlim',Xlim,'Ylim',Ylim)
                        drawnow
                    end
                    
            end
        end
        % -----------------------------------------------------------------
        % record keeping
        
        Record(Iteration+1,:)   =   [   Iteration min(Tree(Alive,iLB)) min(Tree(1:nTree,iUB)) sum(Tree(1:nTree,iLive)==1) nTree ] ;
        
            
        OldVol  	= 	TotVol                              ;
        TotVol      =   ComputeVolumes(Tree,Verbose,Vol0,model)	;
        
        %if OldVol==TotVol
        %    method = method+1 ;
        %    if method==3
        %        method=1;
        %    end
        %end
		
        minLB   =   min(Tree(Alive,iLB))    ;
        minUB   =   min(Tree(Alive,iUB))    ;
        BestObj = minUB;
        
		if Verbose>=3 && mod(Iteration,IterDisplay)==0, disp(['  Min UB:                ' num2str(minUB)    ]), end
        if Verbose>=3 && mod(Iteration,IterDisplay)==0, disp(['  Min LB:                ' num2str(minLB)    ]), end
        
        nTree               =   length(Alive)   ;
        Tree(1:nTree,:)     =	Tree(Alive,:)   ;
        Tree(nTree+1:end,:) =   nan             ;   % initialize tree matrix
        
        drawnow
        
    else
        
        if Verbose>=2, disp(['Terminated at : ' num2str(Iteration-1) ]), end  % display
        
        
        minLB   =   min(Tree(Alive,iLB))    ;
        minUB   =   min(Tree(Alive,iUB))    ;
        BestObj = minUB;
        
        OldVol  	= 	TotVol                                      ;
        TotVol      =   ComputeVolumes(Tree,Verbose,Vol0,model)          ;
        
		if Verbose>=3, disp(['  Min UB:             ' num2str(minUB)    ]), end
        if Verbose>=3, disp(['  Min LB:             ' num2str(minLB)    ]), end
        
    end
    
%    pause
    
end

% =========================================================================
% III. Finalization

% -----------------------------------------------------------------
% 1. Fathoming (safety - remove any spurious solution if still present;
% might not be necessary ) 
Alive           =   find(~isnan(Tree(:,iLive)))         		;
nTree           =   length(Alive)   ;
Tree(1:nTree,:) =	Tree(Alive,:)   ;
Tree            =   Tree(1:nTree,:)          ;   % initialize tree matrix
Alive           =   find(Tree(:,iLive))         		;        

if graph
    switch graphtype
        
        case 2
            drawnow
            figure(gcf())
            AliveN = find(Tree(:,iLive))         ;
            low = min(Tree(AliveN,index:2:end));
            upp = max(Tree(AliveN,index+1:2:end));
            plot([1:nib ; 1:nib],[low; upp],'ko-','MarkerFaceColor','k');
            hold on
            set(gca,'Xlim',Xlim,'Ylim',Ylim)
            drawnow
			
%         case 3
%             drawnow
%             figure(199)
%             hold on
%             low     =   Tree(:,iLB);
%             upp     =   Tree(:,iUB);
%             SolX    =   mean(Tree(:,index:index+1),2) ;
%             plot(SolX,[low],'ko','MarkerFaceColor','b');
%             plot(SolX,[upp],'kd','MarkerFaceColor','r');
%             hold on
%             set(gca,'Xlim',Xlim)
%             drawnow
    end
end

Tolerance 	= 	0 									;
fathom      =   Fathoming(Tree,Alive,Tolerance,multicrit)   	;
Tree(Alive(fathom),iLive)   =   0           		; % fathom these nodes
Tree = Tree(Tree(:,iLive)~=0,:) ;

% -----------------------------------------------------------------
% vizualization
if graph>=2
    figure(gcf())
    plot(Record(:,1),Record(:,1+(1:multicrit)),'o-')
    hold on
    plot(Record(:,1),Record(:,1+(1:multicrit)+multicrit),'+-')
    xlabel('Iteration count')
    ylabel('Bounds')
    
    figure ()
    plot(Record(:,1),Record(:,2+2*multicrit),'o-')
    xlabel('Iteration count')
    ylabel('No. Nodes')

    str = 'm' ;
%     str = 'k' ;
if graphtype<3
    VisualTree(Tree,Alive,str,index,opt,nopt,graphtype,model)
end
end
% -----------------------------------------------------------------

% -----------------------------------------------------------------
% 2. Final solution selection and evaluation
if isempty(Alive)
    model         =   model0   ;
    model.IntX = IntX ;
    model.SolX = mean(IntX,2) ;
    disp('Problem is infeasible (No nodes remained alive)')
else
    if Term1
        disp('Early stopping - No solution provided')
        model = [];
    else
        %figure,
%             plot(mean(Tree(:,end-1:end))',Tree(:,end-4:end-3)','o')
        [model,chosen]     =   SelectFromTree(Tree,model0,DistX,options)  ;
        Tree(chosen,iLive) = -3 ;
        if graph && nib<=2
%             VisualLeaf(Tree,chosen,iLive,index,opt,nopt,graphtype)  ;
        end
    end
end

if Verbose , disp('BB algorithm terminated'), end

return
% =========================================================================

function [LB,UB,Xapp]     =   ProcessLeaf(Xleaf,KnotInt0,boundfun,model,KnotDistLimit,Verify)

Xapp       =   min(max(Xleaf,ones(2,1)*KnotInt0(1,:)),ones(2,1)*KnotInt0(2,:))    ;   % the applied subset bounds are adjusted for root node set bounds
%Xapp

if or(any(Xleaf(1,:)>KnotInt0(2,:)),any(Xleaf(2,:)<KnotInt0(1,:))) % if a subset is out of the original subset, ignore it
    LB  =   Inf ;
    UB  =   Inf ;
%     if Verbose, disp('      out of bounds'), end % disp
else
    [LB,UB]     =   boundfun(model,Xapp,KnotDistLimit,0,Verify)                          ;   % obtain both objective bounds
end

% -----------------------------------------------------------------
function Tree            =   StoreLeaf(Tree,iTree,branch,level,opt,LB,UB,Xapp,Xleaf,model)

% compute subset characteristics and store
Dist	=	Ranger(Xapp) 	;
if isfield(model,'integer')
		Dist(model.integer) = floor(Dist(model.integer))+1 ;
end
City            =   sum(Dist(opt))    ;   % city distance
Vol             =   prod(Dist(opt))   ;   % evaluate volume of intervals
Tree(iTree,:) =   [ branch level+1 1 LB UB Vol City Xleaf(:)' ] ;

% -----------------------------------------------------------------
function Tree            =   FathomLeaf(Tree,iTree,iLive,LB,UB,Xleaf,DeltaKnot,model)    

% 3B. Fathom by individual solution aspects
KnotDist        =   Xleaf(2,:)-Xleaf(1,:)         ;   % range of knot intervals
% if isempty(tk)
%     TimeDist        =   KnotDist    ;
% else
%     TimeDist        =   KnotDist*(tk(2)-tk(1))          ;   % only valid for uniform knot distribution!
% end
if LB == Inf                            % Lower bound is infinity
%     disp('Infeasible')
    Tree(iTree,iLive)     =   -2  ;   %   fathom by infeasibility
elseif LB==UB
    Tree(iTree,iLive)     =   -1  ;   %   stop branching as bounds are the same (not really dead)
else
    if isfield(model,'type') && strcmp(model.type,'SCS')
        
        [verified,ListVerified] = SCSverify_Case1(model,Xleaf) ;
        ListVerified(KnotDist<=DeltaKnot+eps) = 1;
    elseif isfield(model,'type') && strcmp(model.type,'SCPP')
		ExDisc  			= 	true 										;
		ListVerified 		=	CheckResolutionSCPP(model,Xleaf',ExDisc) 	;
        ListVerified(KnotDist<=DeltaKnot+eps) = true;
    else
		ListVerified = KnotDist<=DeltaKnot+eps ;
		if isfield(model,'integer')
			ListVerified(model.integer) = KnotDist(model.integer)==0 ;
		end
    end
    
    if all(ListVerified)         % All variables are found with specified knot resolution
        %disp('All verified')
 %         disp('Satisfied resolution')
%         KnotDist
%         DeltaKnot
        %     if all(TimeDist<=DeltaTime)         %   All variables are found with specified knot and time resolutions
        Tree(iTree,iLive) =   -1  ;   %       stop growth of branch (not really dead)
        %     else                                % Solution found within knot resolution
        %         Tree(iTree,iLive) =   2    ;
        %         %   Setting to 2 puts this node at lower priority, all
        %         %   nodes with 1 are branched until there are no nodes
        %         %   with value 1
        %     end
    end
end

% -----------------------------------------------------------------
function TotVol =   ComputeVolumes(Tree,Display,Vol0,model)    

global multicrit

if isfield(model,'multicrit')
    multicrit = model.multicrit;
else
    multicrit =1;
end

Indices

Inc                   =   find(~isnan(Tree(:,1)))        ;

Alive                   =   find(Tree(Inc,iLive))       ;   % find live nodes
Alive = Inc(Alive) ;
TotCity                 =   sum(Tree(Alive,iCity))          ;
TotVol                  =   sum(Tree(Alive,iVol))           ;

AliveResol                   =   find(Tree(Inc,iLive)==1)   ;   % find live nodes
TotVolResol                  =   sum(Tree(AliveResol,iVol))           ;


Lowest                  =   min(Tree(Alive,index:2:end),[],1)	;
Highest                 =   max(Tree(Alive,index+1:2:end),[],1) ;
Range                   =   (Highest-Lowest)                    ;
if isfield(model,'integer')
    Range(model.integer) = floor(Range(model.integer)) +1 ;
end
TotVolBounding = prod(Range) ;

if Display>=3, disp(['  Volume:                ' num2str(TotVol)   ' of ' num2str(Vol0)  ' [' num2str(TotVol/Vol0*100,'%3.3f')   '%]' ]), end
if Display>=2, disp(['  Volume to split:       ' num2str(TotVolResol)   ' of ' num2str(Vol0)  ' [' num2str(TotVolResol/Vol0*100)   '%]' ]), end
if Display>=3, disp(['  Volume bounding box:	' num2str(TotVolBounding)   ' of ' num2str(Vol0)  ' [' num2str(TotVolBounding/Vol0*100)   '%]' ]), end

% -----------------------------------------------------------------
