function [Feasible,Xpath] = DynProg(K,L,X)

% -------------------------------------------------------------------------
% SPIKE_O toolbox - DynProg.m
%
% This function solves a dynamic program.
%
% Syntax:
%   [Feasible,Xpath] = DynProg(K,L,X)
%
% Inputs:
%   K           :   Feasible states at each time step
%   L           :   Distance constraints
%   X           :   All possible states
%
% Outputs:
%   Feasible    :   A feasible path is found
%   Xpath       :   Obtained path
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2016-01-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


nk      =   length(K)       ;   % number of steps
for ik=1:nk % for each step, add 1
    K{ik}   =   K{ik}+1         ;
end
X       =   [ X(1)-1 ; X(:) ]    ;   % add start node
L       =   [ 1 Inf  ; L ]  ;   % add interval for added node

X       =   X(:)            ;   % as column vector
nx      =   length(X)       ;   % number of nodes
ixx     =   1:nx            ;   % index of nodes

% =====================================
% FORWARD PASS

% initialize
D       =   inf(nx,1)       ;   % accumulated distance
P       =   zeros(nx,1)     ;   % previous node
Pointer =   zeros(nx,nk)    ;   % pointers
D(1)    =   0               ;   % distance to first node = 0

% distance                =   (X(:)*ones(1,nx))'-X(:)*ones(1,nx)  ;
% distance(distance<=0)   =   Inf                                 ;
% distance

for ik=1:nk % run over each step
    dmin            =   inf(nx,1)           ;   % minimum distance to given nodes
    Dnew            =   D                   ;   % set new acc distance to existing distance
    target          =   K{ik}               ;   % target nodes in this step
    for tg=target       % for all target nodes
%         d           =   distance(:,tg)      ;
        d              =   X(tg)-X(:)       ;   % compute distance from each node to target node
        d(d<L(ik,1))   =   inf              ;   % set to inf if distance is too low
        d(d>L(ik,2))   =   inf              ;   % set to inf if distance is too high
        d           =   d.^2                ;   % make quadratic distance
        d           =   D+d                 ;   % add to accumulated distance to each node
        [dm,im]     =   min(d)              ;   % find minimum
        dmin(tg)    =   dm                  ;   % minimum distance to target node
        P(tg)       =   im                  ;   % previous node to reach target node
        Dnew(tg)    =   D(im)+dmin(tg)      ;   % save new distance
    end
    Dnew(setdiff(ixx,target))   =   Inf     ;   % non-target nodes have infinity
    D                           =   Dnew    ;   % save accumulated distance
    Pointer(:,ik)               =   P       ;   % save pointers
end

% =====================================
% BACKWARD PASS
Feasible                =   ~isinf(D(end))  ;   % if distance to last node is infinite,
MyPath                  =   []              ;   % initialize node index
if Feasible                                     % if a path is found
    MyPath(nk)          =   nx              ;   % set last node in path to final index
    for ik=nk:-1:2                                      % run down through path
        MyPath(ik-1)    =   Pointer(MyPath(ik),ik)  ;   % find previous node index
    end
end
Xpath                   =   X(MyPath)       ;   % convert index to value

