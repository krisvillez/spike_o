function IntX = ContractBound(model,options,IntX,DistX,X0)

% -------------------------------------------------------------------------
% SPIKE_O toolbox - ContractBound.m
%
% This function executes the branch-and-bound algorithm.
%
% Syntax:
%   IntX    = ContractBound(model,options,IntX,DistX,X0)
%
% Inputs:
%   model   :   mathematical problem as a structure
%   options :   [optional] options for algorithm execution
%   IntX    :   Root set
%   DistX   :   Distance constraints
%   X0      :   Initial guess
%
% Outputs:
%   IntX    :   Contracted set
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2013-11-13
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------



nvar = size(IntX,2) ;

bounds      =   options.boundfun    ;
boundupp    =   options.boundupp    ;
boundlow    =   options.boundlow    ;
Verbose     =   options.verbose     ;

graph = 1;

if graph

    figure(99)
    close 99
    Lim     =   IntX      ;
    
%     if nvar==1
%         graphtype = 1;
%         nsub = 1;
%         figure(99)
%         subplot(nsub,1,1)
%         AxisPrep
%         hold on
%         Xlim    =   Lim(:,1)    ;
%         Ylim    =   [ 0; 1]     ;
%         switch graphtype
%             case 1
%                 set(gca,'Xlim',Xlim,'Ylim',Ylim)
%                 drawnow
%             case 2
%                 set(gca,'Xlim',Xlim )
%                 drawnow
%         end
%     elseif nvar==2
%         graphtype = 1;
%         figure(99)
%         AxisPrep
%         hold on
%         Xlim    =   Lim(:,1)    ;
%         Ylim    =   Lim(:,2)    ;
%         axis equal
%         drawnow
%         set(gca,'Xlim',Xlim,'Ylim',Ylim)
%         drawnow
%     else
        graphtype = 2;
        
        figure(99)
%        AxisPrep
        hold on
        Xlim    =   [.5 nvar+.5]    ;
        Ylim    =   [ min(IntX(1,:)) max(IntX(2,:)) ]   ;
        drawnow
        set(gca,'Xlim',Xlim,'Ylim',Ylim)
        low = min(IntX,[],1);
        upp = max(IntX,[],1);
        CurrentRange = [ low; upp];
        han1 = plot([1:nvar ; 1:nvar],[low; upp],'ko-','MarkerFaceColor','k');
        drawnow
%     end

end

resolution = 10^(-2) ;

incvar = find(Ranger(IntX)>=resolution);
Converged= false ;
Iteration = 0;

Vol0                  =   prod(Ranger(IntX(:,incvar)))           ;
    
while Iteration<5 && ~Converged
    
    [LB,Jo,Xo]=bounds(model,IntX);
	Jo
    
    Converged   =   true    ; 
    Iteration       =   Iteration+1 ;
    
    if Verbose, disp(['Contracting step: ' num2str(Iteration) ]), end  % display
    
    for ivar=incvar
%         ivar

        
        for dir=1:2
            
    
            switch dir
                case 1 % improve bounds to right hand side
                    inside = 1 ;
                    outside = 2 ;
                    if Verbose, disp(['     Variables: ' num2str(ivar) ' of ' num2str(size(IntX,2)) ' - RHS']), end  % display
                case 2 %improve bounds to the left hand side
                    inside = 2;
                    outside =1;
                    if Verbose, disp(['     Variables: ' num2str(ivar) ' of ' num2str(size(IntX,2)) ' - LHS']), end  % display
            end
            
            alphaL  =   0   ;
            alphaU  =   1   ;
            XA          =   IntX        ;
            XA(inside,ivar)   =   Xo(ivar)     ;
            dx = XA(2,:)-XA(1,:) ;
            if all(abs(dx(incvar))>=resolution)
                [JL]        =   boundlow(model,XA)   ;
                i=0;
                clear Log
                Log(i+1,:)  =   [i JL  0 XA(inside,ivar) ];
                
                imax = 5;
                for i=1:imax
                    if Verbose, disp(['     	Step: ' num2str(i) ' of ' num2str(imax) ]), end  % display
                    alpha       =  (alphaL+alphaU)/2   ;
                    
                    %         XA          =   Xint        ;
                    dx = alpha * ( IntX(outside,ivar) -  Xo(ivar) ) ;
                    XA(inside,ivar)   =   Xo(ivar)    + alpha * ( IntX(outside,ivar) -  Xo(ivar) ) ;
                    dx = XA(2,:)-XA(1,:) ;
                    if all(abs(dx(incvar))>=resolution)
                        [JL]     =   boundlow(model,XA)   ;
                        JL
                        
                        Log(i+1,:) = [i JL  alpha XA(inside,ivar) ];
                        %     figure(i+1), plot([XA(1,:); P2.x'; XA(2,:) ]')
                        if JL>=Jo
                            alphaU  =   alpha   ;
                        else
                            alphaL  =   alpha   ;
                        end
                    end
                end
            end
            if alphaU<1
                Converged = false;
            end
            IntX(outside,ivar)   =   Xo(ivar)    + alphaU *  (  IntX(outside,ivar) -  Xo(ivar)  ) ;
            
        end
    end
    
    if graph
        delete(han1)
        
        low = min(IntX,[],1);
        upp = max(IntX,[],1);
        CurrentRange = [ low; upp];
        hold off
        han1 = plot([1:nvar ; 1:nvar],[low; upp],'ko-','MarkerFaceColor','k');
        hold on
        han1 = plot([1:nvar  ],Xo(1:nvar),'ro','MarkerFaceColor','r');
        set(gca,'Xlim',Xlim,'Ylim',Ylim)
        drawnow
    end

    TotVol                  =   prod(Ranger(IntX(:,incvar)))           ;
    if Verbose>=2, disp(['  Volume:            ' num2str(TotVol)   ' of ' num2str(Vol0)  ' [' num2str(TotVol/Vol0*100)   '%]' ]), end
        
%     IntX
%     Converged
%     resolution
    incvar = find(Ranger(IntX)>=resolution);
    if isempty(incvar)
        Converged = true ;
    end
end

% if options.rounding
%     IntX = [ floor(IntX(1,:)); ceil(IntX(2,:))];
% end

