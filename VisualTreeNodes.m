function VisualTreeNodes(Tree,Iteration)

% -------------------------------------------------------------------------
% Spike_O Toolbox - VisualTreeNodes.m
% -------------------------------------------------------------------------
% Description
%
% This function is a visualization aid for the branch-and-bound algorithm
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	VisualTreeNodes(Tree,Iteration)
%
% INPUT
%   Tree    	:	Solution tree
%	Iteration	:	Current branching iteration
%
% OUTPUT
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-01-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

Indices

colour      =   true    ;   % use colour (1/true) or not (0/false)
colour      =   false    ;   % use colour (1/true) or not (0/false)
if ~colour 
    colour	=	ones(1,3)*.42   ;
    Alpha = 1 ;
else
    colour	=	'b'  ;
    Alpha = 1 ;
end

% IndexLive 	= 	find(Tree(:,iLive)~=1)	;
% X  			=   Tree(IndexLive,index+[0 1 1 0])'  ;
% Y  			=   repmat([0 0 1 1]',[1 size(X,2)])+Iteration                      ;
% patch(X',Y'-.5,[ones(1,3)]*.73,'EdgeColor','k')


IndexLive 	= 	find(Tree(:,iLive)==1)	;
x1  		=   floor(Tree(IndexLive,index+0)'-.5)  ;
x2          =   floor(Tree(IndexLive,index+1)')  ;
X           =   [x1 ; x2 ;  x2 ;  x1] ;
Y  			=   repmat([0 0 1 1]',[1 size(X,2)])+Iteration                      ;
patch(X+.5,Y-.5,colour,'EdgeColor','k','FaceAlpha',Alpha)


IndexLive 	= 	find(Tree(:,iLive)==-1)	;
x1  		=   floor(Tree(IndexLive,index+0)'-.5)  ;
x2          =   floor(Tree(IndexLive,index+1)')  ;
X           =   [x1 ; x2 ;  x2 ;  x1] ;
Y  			=   repmat([0 0 1 1]',[1 size(X,2)])+Iteration                      ;
patch(X+.5,Y-.5,colour,'EdgeColor','k','FaceAlpha',Alpha)


IndexLive 	= 	find(Tree(:,iLive)==-3)	;
x1  		=   floor(Tree(IndexLive,index+0)'-.5)  ;
x2          =   floor(Tree(IndexLive,index+1)')  ;
X           =   [x1 ; x2 ;  x2 ;  x1] ;
Y  			=   repmat([0 0 1 1]',[1 size(X,2)])+Iteration                      ;
patch(X+.5,Y-.5,'r','EdgeColor','k','FaceAlpha',Alpha)



set(gca,'Ylim',[-0.5 Iteration+.5],'Ytick',[0:Iteration]);
