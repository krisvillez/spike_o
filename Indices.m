
% -------------------------------------------------------------------------
% SPIKE_O toolbox - Indices.m
%
% This script provides common index variables
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2016-01-26
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% index variables indicate columns for a given variables
iParent     =   1                           ;
iLevel      =   2                           ;
iLive       =   3                           ;

% if ~exist('multicrit')
%     multicrit = 1;
% end
iLB         =   3+(1:multicrit)             ;
iUB         =   3+(1:multicrit)+multicrit   ;

iVol        =   iUB(end)+1             ;
iCity       =   iVol+1             ;
index       =   iCity+1             ;
