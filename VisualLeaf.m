function VisualLeaf(Tree,iTree,iLive,index,opt,nopt,graphtype)

% -------------------------------------------------------------------------
% Spike_O Toolbox - VisualLeaf.m
% -------------------------------------------------------------------------
% Description
%
% This function is an visualization aid for the branch-and-bound algorithm
%
% -------------------------------------------------------------------------
% Syntax
%
% I/O:	VisualLeaf(Tree,iTree,iLive,index,opt,nopt,graphtype)
%
% INPUT
%   Tree    	:	Solution tree
%	iTree 		:	Index of nodes in current tree to be visualized
%	iLive 		:	Index indicating the column in Tree which specifies whether node is alive or dead
%	index		:	Index indicating first column in Tree representing a bounding value
%	opt			:	Indices of variables which are subject to optimization
%	nopt		:	Number of variables which are subject to optimization
%	graphtype	:	Integer indicating type of graph. Choices are 1 and 2.
%
% OUTPUT
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2015-08-18
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


Indices
    
colour      =   true    ;   % use colour (1/true) or not (0/false)
%colour      =   false    ;   % use colour (1/true) or not (0/false)
switch Tree(iTree,iLive) 
    case -3
        str = 'k'   ;
    case -2
        str = 'c'   ;
    case -1
        str = 'm'   ;
    otherwise
        str = 'y'   ;
end

if ~colour
    if ~strcmp(str,'k')
        str = 'w' ;
    end
end

if Tree(iTree,iLive)  ~=  1,  Alpha   =   1   ;
else                            Alpha   =  0   ;
end
if ~colour && Tree(iTree,iLive) ~= -3 
    Alpha = 0;
end

if nopt==1
    figure(99)
    xx  =   Tree(iTree,index+(opt(1)-1)*2+[0 1 1 0])'  ;
    switch graphtype
		case {1,2}
			switch graphtype
				case 1
					yy  =   [0 0 1 1]'                      ;
				case 2
					LB  =   Tree(iTree,iLB) ;
					UB  =   Tree(iTree,iUB) ;
					yy  =   [LB LB UB UB]'                              ;
			end
			patch(xx,yy,str,'EdgeColor','k','FaceAlpha',0)
		case 3
			LB  =   Tree(iTree,iLB) ;
			UB  =   Tree(iTree,iUB) ;
			subplot(2,1,2)
			plot(xx(1:2),[UB UB],'r--')
			plot(xx(1:2),[LB LB],'b-')
    end
    
    drawnow
elseif nopt>=2
    figure(99)
    xx  =   Tree(iTree,index+(opt(1)-1)*2+[0 1 1 0])'  ;
    yy  =   Tree(iTree,index+(opt(2)-1)*2+[0 0 1 1])' ;
    patch( xx,yy,str,'EdgeColor','k','FaceAlpha',Alpha)
    drawnow
end
